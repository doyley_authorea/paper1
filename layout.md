begin_abstract_end_abstract__.tex
section_Introduction_Plane_wave_imaging__.tex
section_Beamforming_Approaches_The_backscatter__.tex
subsection_Methods_and_Materials_subsection__.tex
section_Results_subsection_Simulation_Study__.tex
section_Conclusion_ddd__.tex
section_Figures__.tex
figures/ApodBF/ApodBF.png
figures/PSFMontage/PSFMontage.png
figures/PSFProfile/PSFProfile.png
figures/B-modeMontage/B-modeMontage.png
figures/BmodeMetrics/BmodeMetrics.png
figures/GroundTruth/GroundTruth.png
figures/SimRadMontage/SimRadMontage.png
figures/SimStrainAnalysis/SimStrainAnalysis.png
figures/ConditionNumber/ConditionNumber.png
figures/TapComp/TapComp.png
figures/TimeOfComputation/TimeOfComputation.png
