\subsection{Methods and Materials}

\subsection{Simulation Study}
We generated the radio-frequency echo data for simulation study in two steps -(1) mechanical and (2) acoustic modeling.

\subsubsection{Mechanical models}
Finite element models of diseased arteries were created with Abaqus/CAE (Dassault Systems, Veliz-Villacoublay, France), a commercially available finite-element software package.The heterogeneous vessel model had inner and outer radii of 1.5 and 6 mm, respectively,  containing a soft plaque that was assigned Young’s modulus of 12.5 kPa while Young’s modulus of the artery wall was 50 kPa. A Poisson’s ratio of 0.495 was assigned to all elements in both regions. These mechanical parameters were chosen based on studies reported in \cite{hansen}. The vessels were pressurized by applying a uniform pressure of 333.4, 666.7, 1000, 1333.4 and 1666.7 Pa (≈ 2.5, 5, 7.5, 10, 12.5  mmHg) to the nodes on the inner lumen. Based on the applied intraluminal pressure, the model was deformed and the corresponding displacements and strains induced in the object were recorded. 
\subsubsection{Acoustic models}
The ultrasound simulation package Field II \cite{jenseni} was used to generate the acoustic response of the L14-5/38 linear transducer array
(Prosonic Corporation, Korea) used in the experimental studies
when operating at 5 MHz. The echo response of the pre-deformed artery was simulated by randomly distributing point scatterers (16 scatterers per wavelength) within the simulated vessel to generate fully developed speckles \cite{Wagner}. To simulate the acoustic response of the post-deformed vessel, point scatterers were redistributed according to the displacement fields computed using FEM. The speed of sound in the vessel models was set to 1540 $ms^{-1}$.  In case of compounded plane wave, 15 sets of pre- and post-deformed plane-wave data were produced by steering the beams in the range of $-14^\circ$ to $14^\circ$ in increments of $2^\circ$. We used an additive Gaussian white noise model to simulate RF echo frames with signal-to-noise ratio of 20 dB. These SNR values represent those that were measured experimentally when our US system was configured to acquired RF echo frames with PW imaging settings. 
\subsection{Phantom Study}
The goal of phantom studies was to corroborate the findings of the
simulation study under controlled experimental conditions. 
\subsubsection{Phantom Fabrication}
We fabricated vessel phantom (20 mm outer diameter by 10 mm long) containing a soft,crescent-shaped inclusion  from polyvinyl alcohol (PVA, Elvanol 71-30, Dupont, Wilmington, Delaware) and silicon carbide (320 Grit, Fisher Scientific, Fair Lawn, New Jersey). We used  silicon carbide (SiC) to control the echogenicity of the vessels as described in \cite{maurice}, \cite{fromageau} and \cite{richards}. The fabrication process comprises of two steps. First, a solution of 10\%  by weight PVA (DuPont Inc., Wilmington, DE) and 2\% by weight carborundum (SiC, with diameters of 3 to 10 $\mu m$) was poured into a cylindrical mold (20 mm diameter by 10 mm long) that contained an off-center rod (6.32 mm diameter). The mold was then subjected to two freeze-thaw cycles, from $−20^\circ C$ to $+20^\circ C$, over a 48-h (24 h per cycle) period. Next, the central rod was removed and replaced with a 3.16-mm-diameter rod. The vacant cavity between the vessel wall and rod was filled with 10\% by weight PVA, and the phantom was subjected to two freeze-thaw cycles. After the thermal cycling, the phantom was removed from the mold and stored at room temperature in water.A Landmark Servo-hydraulic Test System (MTS Systems Corp., Eden Prairie, MN) and a 5-lb load cell were used to measure the mechanical properties of representative samples of the vessel wall and plaque. All measurements were performed on five homogeneous cylindrical shaped samples (20 mm diameter by 10 mm height) at room temperature ($−20^\circ C$). The Young’s modulus of the vessel wall was 41.6 $\pm$ 0.3 kPa, and that of the plaque was 19.9 $\pm$ 0.7 kPa.


\subsubsection{Elastographic Data Acquistion}

The equipment used for elastographic imaging consisted of a
Sonix RP US system (Ultrasonix, Peabody, Massachusetts)
that was equipped with a 128 element linear transducer array (L14-5/38 probe), a multichannel data acquisition system
(Sonix DAQ®, Ultrasonix, Peabody, Massachusetts), a simple
water column system, and a pressure wire (Millar Instruments
Mikro-Cath, Houston, Texas).

We used software development kit(TEXO SDK, v5.6.6, Ultrasonix Medical Corp.) to configure US system to acquire 15 sets of PW images by steering the beam between $−14^\circ$ and $−14^\circ$ at the increments of $2^\circ$. All studies were performed in a water tank measuring 15 x 15 x 10 cm, which was filled with degassed water. We used the water column system to pressurize the vessels to 666.61 and 1000 Pa (5 and 10 mmHg).  All echo imaging was performed at 5 MHz, and the received signal was sampled to 12 bits at 40 MHz. The delay-sum technique was used to beam-form PW data as described in the section 2.


\subsection{Displacement and Strain estimation}
Axial (dz) and lateral (dx) displacements were estimated by applying a 2-D echo-tracking technique \cite{doyley} to the pre-and post-deformed RF echo frames. All echo tracking was performed with 1 X 1 mm kernels that overlapped by 70\% in both coordinate directions. Sub-pixel displacements were estimated by fitting a 2-D parabolic function to the top of the cross-correlation function. A 3 X 3 kernel
median filter was applied to the axial and lateral displacement
elastograms to eliminate spurious displacement estimates. The normal and shear components of the strain tensor ($\epsilon$) were computed by applying a finite difference operator ($\Delta$)to the axial and lateral displacement estimates. We computed radial ($\epsilon_{rr}$) and circumferential ($\epsilon_{\theta \theta}$) strain elastograms by transforming the normal and shear strains to the polar coordinate system $(r,\theta)$ as follows \cite{korukonda}:

\begin{equation}
\epsilon_{rr}=\frac{1}{x^2+z^2}[x^2 \epsilon_{xx} + xz (\gamma_{xz} + \gamma_{zx}) +z^2 \epsilon_{zz}]
\end{equation}

\begin{equation}
\epsilon_{\theta \theta}=\frac{1}{x^2+z^2}[x^2 \epsilon_{xx} - xz (\gamma_{xz} + \gamma_{zx}) +z^2 \epsilon_{zz}]
\end{equation}
where normal axial and lateral strains are given by $\epsilon_{zz}$ and $\epsilon_{xx}$ respectively, while the shear strains in the cartesian domain are given by $\gamma_{xz}$ and $\gamma_{zx}$, respectively.


\section{Statistical data analysis}
We used the elastographic contrast-to-noise ratio ($CNR_e$) and
the normalized root-mean-square error (NRMSE) as quantitative metrics to assess the performance of both strain and modulus elastograms. The
elastographic CNR metric was defined as follows:
\begin{equation}
CNR_e[dB]=20 \log_{10}\frac{2(\mu_b - \mu_p)^2}{\sigma^2_b+\sigma^2_p}
\end{equation}
where $\mu_b$ and $\mu_p$ represent the mean strain in the vessel wall and plaque; and $\sigma_b$ and $\sigma_p$ represent the standard deviation in strain  values in the corresponding regions. The normalized root-mean-squared error (NRMSE) was computed as follows:
\begin{equation}
NRMSE=\frac{\sqrt{\frac{\sum_{i=1}^{N} [x_m(i) - x_c(i)]^2}{N}}}{max[x_c] - min[x_c]}
\end{equation}
where $x_c$ and $x_m$ represent the actual and measured strain respectively, and N is the number of pixels in the region of interest.

We employed Kolmogorov-Smirnov (K-S) Test for comparing the strain images. The K-S test is a non-parametric statistical test which can be used to test the hypothesis that the two images have the same grayscale intensity distributions \cite{Demidenko}. We computed the p-values to investigate the statistical significance of the difference between the images. 
