\section{Plane wave imaging}
The backscatter signal at a point $(x_0,z_0)$ in the reconstructed image is computed as follow:
\begin{equation}
s(x_0,z_0)=\sum_{i=1}^{N_{tx}}\sum_{j=1}^{N_{rx}}w_{ij}X_{ij}[t-\tau(x_0,z_0)]
\end{equation}
where $X_{ij}$ represents the RF echo for $i^{th}$ plane wave transmission and the $j^{th}$ element receive; $t$ represents the time of flight of the echo; $\tau$ represents the
round-trip time from point $(x_0,z_0)$; $w_{ij}$ represents the apodization weight; $N_{tx}$ and $N_{rx}$ represent the number of transmissions and receive elements in the imaging sequence. In case of study of beamfroming approaches based on different apodization functions, we used a single unfocused plane wave transmission at $0^\circ$ for image reconstructions i.e. $N_{tx}$ equals one. The study of plane wave compounding was performed by varying the sector angle ( at increment of $2^\circ$) for $N_{tx} =$ 1, 3, 7, 11 and 15. 
\subsection{Fixed Apodization }
Figure 1(a) shows the conventional scheme of beamforming where each sensor signal is delayed and weighted by fixed weights before summation. We chose the fixed weighting functions (Fig. 1 (d)) in order to study the effect of main-lobe and side-lobes. For a given aperture, under condition that  Fraunhoffer approximation is valid,  lateral profile of the PSF can be approximated as the Fourier transform of the aperture function or apodization [11], as given by:
\begin{equation}
F\{w(x_i)\}=h(\frac{x}{\lambda z})
\end{equation}
where $F\{\}$ is the Fourier transform operator, $w(x_i)$ is the transmit or receive apodization coefficient of element $i$ situated in position $x_i$, $h$ is the transmit or receive lateral profile expressed as a function of $\lambda$, wavelength of the transmitted pulse, $z$ the depth of point of interest and $x$ the lateral position. Therefore, we chose Boxcar function for extreme case with narrowest mainlobe and highest sidelobe levels. On the other extreme, Prolate spheroidal wave functions (PSWF),  have very low side-lobes as demonstrated by Parker \cite{parker} but broad main-lobe. In between these two extremes, we chose the modified hyperbolic sine functions introduced by Parker to control the main-lobe width and side-lobe levels. These functions are defined over a limited domain that can be easily manipulated using single parameter and are given by:

 
 \[
     F(x)= 
 \begin{cases}
     (\sinh [1-x^2])^\alpha ,& \text{if } -1 \geq x\leq 1\\
     0,              & \text{otherwise}
 \end{cases}
 \]
For the particular case where $\alpha=5$ and $c=1$, this function was found to be “approximate eigenfunction” of the Fourier transform (FT) operation, and  closely resembles a spheroidal function $PS0,0[14,x]$. FWHM of the apodization curves can be varied by changing the value of $c$. For the comparison, we used $c=1.5, 2.0 and 2.5$ for following reasons. For value of $c$ equal to one, it becomes the PSWF and for $c > 2.5$, the side-lobes of corresponding PSF were as high as that of boxcar function. 

\subsection{Adaptive Apodization}
Figure. 1 (b) illustrates the MV beamforming process. In general,
for a MV beamformer with $N$ receive channels, the adaptive
apodization weights $w$ can be computed through the following equation:
\begin{equation}
w=\frac{R^{-1}a}{a^H R^{-1} a}
\end{equation}
where $R^{-1}$ is the inverse of the covariance matrix for focus-delayed samples given by:
\begin{equation}
R(t)=E[X(t) X(t)^H]
\end{equation}
and $a$ is simply a vector of ones because the channel data being processed are already delayed. However in practice, the covariance matrix is ill-conditioned since the delayed broadband data consists of single or very few samples. Therefore, the covariance matrix $R$ is usually estimated by spatial averaging, in which array is divided into overlapping subarrays and the resulting covariance matrices are averages. The estimated covariance matrix then becomes:
 
\begin{equation}
R(t)=\frac{1}{M-L+1} \sum_{k=0}^{M-L} x_k(t) x_k^H (t) 
\end{equation}
where $x_k(t)$ is a $L \times 1$ vector of delayed echo samples in the $k^{th}$ sub-aperture, which starts from $k^{th}$ channel to $(k +
L − 1)^{th}$ channel. Furthermore, to ensure that the covariance
matrix is invertible, the sub-aperture size is limited to $L \leq M/2
$ . Subsequently, the amplitude estimate $z(t)$ is obtained by averaging the weighted sum of focus-delayed echo signals from all sub-aperture channels.
\begin{equation}
z(t)=\frac{1}{M-L+1}\sum_{k=0}^{M-L} w^H(t) x(t)
\end{equation}
MV beamformer achieves high resolution through only
allowing reflections from receive focal point to pass through
the beamformer with unity gain while others are suppressed.
Yet, it is sensitive to wavefield parameters. For example,
inaccurate delay calculation due to phase aberration could lead
to underestimation of the amplitude estimates. By increasing
robustness of the beamformer, the suppression of slight out of
focus reflection can be tuned. To improve robustness, one could reduce the sub-aperture length L. It should be noted that increasing the robustness of the beamformer would degrade the resolution of the output image. Therefore, a tradeoff has to be made. For present study, we compared the performance of MV beamformer corresponding to  L=1, 8, 16, 32 and 48. We chose these values within the allowable range of subarray lengths while avoiding the higher values.  

\subsection{Compounded Plane Wave}
We performed plane wave compounding corresponding to maximum angles of $0^\circ$,$2^\circ$, $6^\circ$, $10^\circ$ and $14^\circ$ at the increments of $2^\circ$.To simulate beam steering, linear delays were applied to each element in the transducer array. For a given insonification angle $\theta$, the delay $dt$ for each
element was calculated as follows (Von Ramm and W., 1983):
\begin{equation}
dt(n)=n d \sin(\theta)
\end{equation}
where $d$ is the pitch of the transducer and $n$, ranging from $0$ to $N-1$, is the array element number. When obtaining plane wave images at a steering angle of $\theta$, the time taken for the plane wave to travel to the given point was computed as:
\begin{equation}
\tau_{tx}=(x\sin(\theta)+z\cos(\theta)+\frac{W}{2}\sin(|\theta|)/c
\end{equation}
where $W$ is the width of the transducer array. These calculations are made by assuming that the origin of all reconstructed images lies at the center of the transducer array. The return trip time ($\tau_{rx}$)  was computed as:
\begin{equation}
\tau_{rx}=(\sqrt{(x_j-x)^2+z^2})/c
\end{equation}
where $x_j$ is the location of the receiving element. The complete round trip time was given by:
\begin{equation}
\tau(x,z)=\tau_{tx}+\tau_{rx}
\end{equation}
To generate a composite angularly compounded image, the plane wave
images obtained at each angular insonification angle are spatially added to one another as described in \cite{montaldo}.
